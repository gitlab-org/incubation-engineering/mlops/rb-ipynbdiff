# frozen_string_literal: true

lib = File.expand_path('lib/..', __dir__)
$LOAD_PATH.unshift lib unless $LOAD_PATH.include?(lib)

require 'lib/version'

Gem::Specification.new do |s|
  s.name        = 'ipynbdiff'
  s.version     = IpynbDiff::VERSION
  s.summary     = 'Human Readable diffs for Jupyter Notebooks'
  s.description = 'Better diff for Jupyter Notebooks by first preprocessing them and removing clutter'
  s.authors     = ['Eduardo Bonet']
  s.email       = 'ebonet@gitlab.com'
  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  s.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec|example)/}) }
  end
  s.homepage =
    'https://gitlab.com/gitlab-org/incubation-engineering/mlops/rb-ipynbdiff'
  s.license       = 'MIT'

  s.require_paths = ['lib']

  s.add_runtime_dependency 'diffy', '~> 3.3'
  s.add_runtime_dependency 'json', '~> 2.5', '>= 2.5.1'

  s.add_development_dependency 'bundler', '~> 2.2'
  s.add_development_dependency 'guard-rspec'
  s.add_development_dependency 'pry'
  s.add_development_dependency 'rake'
  s.add_development_dependency 'rspec'
  s.add_development_dependency 'rspec-parametized'
  s.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
